#include "vector.h"

namespace optimized {
    template<typename T, typename Alloc>
    vector<T, Alloc>::vector(const vector <T, Alloc> &other) noexcept
            : _size(other._size), is_small(other.is_small), storage(is_small, other.storage) {}

    template<typename T, typename Alloc>
    vector<T, Alloc>::vector(vector <T, Alloc> &&other) noexcept : vector() {
        set_from(other);
    }

    template<typename T, typename Alloc>
    vector<T, Alloc>::vector(size_type size, const_reference value) : _size(size), is_small(size <= SMALL_SIZE) {
        if (not is_small) {
            storage.update(true, size);
        }
        std::uninitialized_fill_n(data(), size, value);
    }

    template<typename T, typename Alloc>
    template<typename It>
    vector<T, Alloc>::vector(It first, It last): vector() {
        range_initialize(first, last);
    }

    template<typename T, typename Alloc>
    vector<T, Alloc>::vector(std::initializer_list<value_type> l) : vector() {
        reserve(l.size());
        range_initialize(l.begin(), l.end());
    }

    template<typename T, typename Alloc>
    void vector<T, Alloc>::reserve(size_type new_size) {
        if (new_size > capacity()) {
            auto stored_data = store_data_and_extend_large_storage(new_size);
            std::uninitialized_move(stored_data.get(), stored_data.get() + size(), storage.large.data.get());
        }
    }

    template<typename T, typename Alloc>
    void vector<T, Alloc>::clear() noexcept {
        about_to_modify(0, false);
        std::destroy(begin(), end());
        _size = 0;
    }

    template<typename T, typename Alloc>
    typename vector<T, Alloc>::iterator
    vector<T, Alloc>::insert(const_iterator pos, size_type count, const T &value) {
        about_to_modify(0, true);
        auto offset = pos - begin();
        auto new_size = size() + count;
        if (count + size() < capacity()) {
            std::move_backward(begin() + offset, end(), end() + count);
            std::fill_n(begin() + offset, count, value);
        } else {
            auto stored = store_data_and_extend_large_storage(new_size);
            auto it = std::uninitialized_move(stored.get(), stored.get() + offset, storage.large.data.get());
            it = std::uninitialized_fill_n(it, count, value);
            std::uninitialized_move(stored.get() + offset, stored.get() + size(), it);
        }
        _size = new_size;
        return begin() + offset;
    }

    template<typename T, typename Alloc>
    typename vector<T, Alloc>::iterator
    vector<T, Alloc>::erase(iterator first, iterator last) {
        about_to_modify(0, true);
        auto diff = last - first;
        auto pos = first - begin();
        auto it = std::move(last, end(), first);
        std::destroy(it, end());
        _size -= diff;
        return begin() + pos;
    }

    template<typename T, typename Alloc>
    void vector<T, Alloc>::about_to_modify(vector<T, Alloc>::size_type new_size, bool mark_unshareable) {
        if (not is_small and not storage.large.unique()) {
            storage.update(is_small, storage.large.copy(size(), std::max(new_size, size())));
        } else {
            reserve(new_size);
        }
        if (not is_small) {
            storage.large.unshareable = mark_unshareable;
        }
    }

    template<typename T, typename Alloc>
    template<typename ... Args>
    typename vector<T, Alloc>::reference
    vector<T, Alloc>::emplace_back(Args &&... args) {
        about_to_modify(size() + 1, true);
        std::allocator_traits<Alloc>::construct(allocator, (data() + size()), std::forward<Args>(args)...);
        ++_size;
        return back();
    }

    template<typename T, typename Alloc>
    void vector<T, Alloc>::resize(size_type new_size) {
        about_to_modify(new_size, false);
        if (new_size < size()) {
            std::destroy(data() + new_size, data() + size());
        } else {
            auto temp = std::make_unique<value_type[]>(new_size - size());
            std::move(temp.get(), temp.get() + new_size - size(), end());
        }
        _size = new_size;
    }

    template<typename T, typename Alloc>
    void swap(vector <T, Alloc> &one, vector <T, Alloc> &two) { one.swap(two); }

}
