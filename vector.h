#ifndef OPTIMIZED_VECTOR_H
#define OPTIMIZED_VECTOR_H

#include <cstddef>
#include <cstring>
#include <limits>
#include <memory>

namespace optimized {
    template<typename T>
    class shared_array_ptr;

    template<typename T>
    class shared_array_ptr<T[]> {
    public:
        using value_type = T;
        using pointer = T *;
        using const_pointer = T const *;

        shared_array_ptr() : data(nullptr), count(new unsigned(1)) {}

        ~shared_array_ptr() {
            --*count;
            if (*count == 0) {
                operator delete(data);
                delete count;
            }
        }

        explicit shared_array_ptr(std::nullptr_t null) : data(null), count(new unsigned(1)) {}

        explicit shared_array_ptr(value_type data[]) : data(data), count(new unsigned(1)) {}

        shared_array_ptr(shared_array_ptr const &other) noexcept : data(other.data), count(other.count) {
            ++*count;
        }

        shared_array_ptr(shared_array_ptr &&other) noexcept : shared_array_ptr() { swap(other); }

        shared_array_ptr &operator=(shared_array_ptr rhs) {
            swap(rhs);
            return *this;
        }

        pointer get() { return data; }

        const_pointer get() const { return data; }

        unsigned use_count() const { return *count; }

        void swap(shared_array_ptr &other) {
            using std::swap;
            swap(data, other.data);
            swap(count, other.count);
        }

    public:
        pointer data;
        unsigned *count;
    };

    template<typename T>
    struct buffer { // TODO: should I put here only definition
        using size_type = std::size_t;
        using value_type = T;

        size_type capacity;
        bool unshareable;
        shared_array_ptr<value_type[]> data;
    public:
        buffer() : capacity(0), unshareable(false), data(nullptr) {};

        explicit buffer(size_type capacity)
                : capacity(capacity)
                  , unshareable(false) {
            auto temp = (value_type *) operator new(capacity * sizeof(T));
            data = shared_array_ptr<value_type[]>(temp);
        };

        buffer(buffer const &other) : capacity(other.capacity), unshareable(false) {
            if (other.unshareable) {
                *this = other.copy(capacity, capacity); // TODO: copies more than should, maybe introduce size field
            } else {
                data = other.data;
            }
        }

        buffer(buffer &&other) noexcept : capacity(other.capacity), unshareable(false), data(other.data) {}

        buffer &operator=(buffer other) {
            set_from(other);
            return *this;
        }

        bool unique() { return data.use_count() == 1; }

        buffer copy(size_type size, size_type capacity) const noexcept {
            buffer result(capacity);
            std::copy(data.get(), data.get() + size, result.data.get());
            return result;
        }

        void set_from(buffer &other) {
            capacity = other.capacity;
            unshareable = other.unshareable;
            data = other.data;
        }
    };

    template<typename T, typename Allocator = std::allocator<T>>
    class vector {
    public:
        using allocator_type = Allocator;
        using size_type = typename allocator_type::size_type;
        using value_type = typename allocator_type::value_type;
        using reference = typename allocator_type::reference;
        using const_reference = typename allocator_type::const_reference;
        using pointer = typename allocator_type::pointer;
        using const_pointer = typename allocator_type::const_pointer;
        using iterator = T *;
        using const_iterator = T const *;
        using reverse_iterator = std::reverse_iterator<iterator>;

    private:
        static constexpr double EXPANSION_COEFFICIENT = 1.75;

        using large_type = buffer<value_type>;
        static constexpr size_type SMALL_SIZE = sizeof(large_type) / sizeof(value_type);
        using small_type = value_type[SMALL_SIZE];

        static_assert(sizeof(large_type) == sizeof(small_type));

    private:
        allocator_type allocator;

        size_type _size;
        bool is_small;

        union storage_type {
            large_type large;
            small_type small;

            explicit storage_type(bool is_small = true) noexcept : small{} {
                if (not is_small) {
                    new(&large) large_type();
                }
            }

            storage_type(bool is_small, storage_type const &other) noexcept {
                if (is_small) {
                    std::uninitialized_copy(other.small, other.small + SMALL_SIZE, small);
                } else {
                    new(&large) large_type(other.large);
                }
            }

            storage_type(bool is_small, storage_type &&other) noexcept {
                if (is_small) {
                    std::uninitialized_move(other.small, other.small + SMALL_SIZE, small);
                } else {
                    new(&large) large_type(other.large);
                }
            }

            ~storage_type() {}

            void assign(bool do_not_destroy_large, bool is_small_rhs, storage_type const &rhs) {
                destroy(do_not_destroy_large);
                new(this) storage_type(is_small_rhs, rhs);
            }

            void destroy(bool do_not_destroy_large) {
                if (not do_not_destroy_large) { large.~large_type(); }
            }

            template<typename ... Args>
            void update(bool do_not_destroy_large, Args &&...args) {
                destroy(do_not_destroy_large);
                new(&large) large_type(std::forward<Args>(args)...);
            }

            void update(bool do_not_destroy_large, small_type &lhs) {
                destroy(do_not_destroy_large);
                std::uninitialized_move(lhs, lhs + SMALL_SIZE, small);
            }

            friend void swap(storage_type &one, bool is_small_one, storage_type &two, bool is_small_two) {
                storage_type temp(is_small_one, one);
                one.assign(is_small_one, is_small_two, two);
                two.assign(is_small_two, is_small_one, temp);
                temp.destroy(is_small_one);
            }

        } storage;

    public:
        vector() noexcept : _size(0), is_small(true), storage{} {}

        vector(vector const &other) noexcept;

        vector(vector &&other) noexcept;

        explicit vector(size_type size, const_reference value = value_type());

        template<typename InputIterator>
        vector(InputIterator first, InputIterator last);

        vector(std::initializer_list<value_type> l);

        ~vector() {
            std::destroy(begin(), end());
            storage.destroy(is_small);
        }

        vector &operator=(vector other) noexcept {
            set_from(other);
            return *this;
        }

        void assign(size_type count, const_reference value) {
            about_to_modify(count, false);
            std::uninitialized_fill_n(data(), count, value);
            erase(begin() + count, end());
        }

        reference operator[](size_type i) noexcept {
            about_to_modify(size(), true);
            return data()[i];
        };

        const_reference operator[](size_type i) const noexcept { return data()[i]; };

        reference back() noexcept {
            about_to_modify(size(), true);
            return *(end() - 1);
        }

        const_reference back() const noexcept {
            return *(end() - 1);
        }

        pointer data() noexcept {
            about_to_modify(size(), true);
            return is_small ? storage.small : storage.large.data.get();
        }

        const_pointer data() const noexcept { return is_small ? storage.small : storage.large.data.get(); }

        iterator begin() noexcept {
            about_to_modify(0, true);
            return iterator(data());
        }

        const_iterator begin() const noexcept { return iterator(data()); }

        iterator end() noexcept {
            about_to_modify(0, true);
            return iterator(data() + size());
        }

        const_iterator end() const noexcept { return const_iterator(data() + size()); }

        const_iterator cbegin() const noexcept { return begin(); }

        const_iterator cend() const noexcept { return end(); }

        reverse_iterator rbegin() {
            about_to_modify(0, true);
            return reverse_iterator(end());
        }

        reverse_iterator rend() {
            about_to_modify(0, true);
            return reverse_iterator(begin());
        }

        bool empty() const noexcept { return size() == 0; }

        size_type size() const noexcept { return _size; };

        size_type capacity() const noexcept { return is_small ? SMALL_SIZE : storage.large.capacity; };

        void shrink_to_fit() {}

        void reserve(size_type new_size);

        void clear() noexcept;

        iterator insert(const_iterator pos, size_type count, T const &value);

        iterator erase(iterator it) {
            return erase(it, it + 1);
        }

        iterator
        erase(iterator first, iterator last);

        void push_back(const value_type &value) {
            about_to_modify(size() + 1, false);
            emplace_back(value);
        }

        void pop_back(const value_type &value) {
            about_to_modify(size() - 1, false);
            erase(end() - 1);
        }

        void about_to_modify(size_type new_size, bool mark_unshareable);

        template<typename ...Args>
        reference emplace_back(Args &&...args);

        void resize(size_type new_size);

        void swap(vector &other) {
            using std::swap;
            swap(_size, other._size);
            swap(storage, is_small, other.storage, other.is_small);
            swap(is_small, other.is_small);
        };

        template<typename Tp, typename Al>
        friend bool operator==(vector<Tp, Al> const &lhs, vector<Tp, Al> const &rhs) {
            return std::equal(lhs.begin(), lhs.end(), rhs.begin(), rhs.end());
        }

    private:
        template<typename It>
        using category = typename std::iterator_traits<It>::iterator_category;

        template<typename It>
        std::enable_if_t<std::is_same_v<category<It>, std::random_access_iterator_tag>, void>
        range_initialize(It first, It last) {
            reserve(last - first);
            for (; first != last; ++first) {
                emplace_back(*first);
            }
        }

        template<typename It>
        std::enable_if_t<not std::is_same_v<category<It>, std::random_access_iterator_tag>, void>
        range_initialize(It first, It last) {
            for (; first != last; ++first) {
                emplace_back(*first);
            }
        }

        shared_array_ptr<value_type[]> store_data_and_extend_large_storage(size_type new_size) {
            shared_array_ptr<value_type[]> copy;
            if (is_small) {
                auto temp = (value_type *) operator new(sizeof(value_type) * size());
                copy = shared_array_ptr<value_type[]>(temp);
                std::uninitialized_move(begin(), end(), copy.get());
            } else {
                copy = storage.large.data;
            }
            auto required = std::max<size_type>(capacity() * EXPANSION_COEFFICIENT, new_size);
            storage.update(is_small, 4 * ((required - 1) / 4 + 1));
            is_small = false;
            return copy;
        }

        void set_from(vector &other) {
            _size = other._size;
            storage.assign(is_small, other.is_small, other.storage);
            is_small = other.is_small;
        }
    };
}

#include "vector.tpp"

#endif